'use strict';
var app = require('../../server/server');
var _ = require('lodash');

module.exports = function(Product) {

  Product.bestrate = function(cb) {
    app.models.product.find(function (err, products) {
      if (err) {
        return cb(err);
      }
      var bestRateProduct = _.orderBy(products, ['rate'], ['desc'])[0];
      var respData = {
        bestProduct: bestRateProduct,
        allProducts: products
      };
      cb(null, respData);
    });
  }

  Product.remoteMethod(
    'bestrate',
    {
      returns: { arg: 'data', type: 'object'},
      http: { path: '/bestrate', verb: 'get'}
    }
  );
};
